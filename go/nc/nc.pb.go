// Code generated by protoc-gen-go. DO NOT EDIT.
// source: nc/nc.proto

package nc

import (
	context "context"
	fmt "fmt"
	gw "gitee.com/dfrobotcd/chirpstack-api/go/gw"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type MType int32

const (
	MType_UNKNOWN               MType = 0
	MType_JOIN_REQUEST          MType = 1
	MType_JOIN_ACCEPT           MType = 2
	MType_UNCONFIRMED_DATA_UP   MType = 3
	MType_UNCONFIRMED_DATA_DOWN MType = 4
	MType_CONFIRMED_DATA_UP     MType = 5
	MType_CONFIRMED_DATA_DOWN   MType = 6
	MType_REJOIN_REQUEST        MType = 7
)

var MType_name = map[int32]string{
	0: "UNKNOWN",
	1: "JOIN_REQUEST",
	2: "JOIN_ACCEPT",
	3: "UNCONFIRMED_DATA_UP",
	4: "UNCONFIRMED_DATA_DOWN",
	5: "CONFIRMED_DATA_UP",
	6: "CONFIRMED_DATA_DOWN",
	7: "REJOIN_REQUEST",
}

var MType_value = map[string]int32{
	"UNKNOWN":               0,
	"JOIN_REQUEST":          1,
	"JOIN_ACCEPT":           2,
	"UNCONFIRMED_DATA_UP":   3,
	"UNCONFIRMED_DATA_DOWN": 4,
	"CONFIRMED_DATA_UP":     5,
	"CONFIRMED_DATA_DOWN":   6,
	"REJOIN_REQUEST":        7,
}

func (x MType) String() string {
	return proto.EnumName(MType_name, int32(x))
}

func (MType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_7dd10f8ea2e14d19, []int{0}
}

type HandleUplinkMetaDataRequest struct {
	// Device EUI (8 bytes).
	DevEui []byte `protobuf:"bytes,1,opt,name=dev_eui,json=devEui,proto3" json:"dev_eui,omitempty"`
	// TX meta-data.
	TxInfo *gw.UplinkTXInfo `protobuf:"bytes,2,opt,name=tx_info,json=txInfo,proto3" json:"tx_info,omitempty"`
	// RX meta-data.
	RxInfo []*gw.UplinkRXInfo `protobuf:"bytes,3,rep,name=rx_info,json=rxInfo,proto3" json:"rx_info,omitempty"`
	// PHYPayload byte count.
	PhyPayloadByteCount uint32 `protobuf:"varint,4,opt,name=phy_payload_byte_count,json=phyPayloadByteCount,proto3" json:"phy_payload_byte_count,omitempty"`
	// MAC-Command byte count.
	MacCommandByteCount uint32 `protobuf:"varint,5,opt,name=mac_command_byte_count,json=macCommandByteCount,proto3" json:"mac_command_byte_count,omitempty"`
	// Application payload byte count.
	ApplicationPayloadByteCount uint32 `protobuf:"varint,6,opt,name=application_payload_byte_count,json=applicationPayloadByteCount,proto3" json:"application_payload_byte_count,omitempty"`
	// Message type.
	MessageType          MType    `protobuf:"varint,7,opt,name=message_type,json=messageType,proto3,enum=nc.MType" json:"message_type,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HandleUplinkMetaDataRequest) Reset()         { *m = HandleUplinkMetaDataRequest{} }
func (m *HandleUplinkMetaDataRequest) String() string { return proto.CompactTextString(m) }
func (*HandleUplinkMetaDataRequest) ProtoMessage()    {}
func (*HandleUplinkMetaDataRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_7dd10f8ea2e14d19, []int{0}
}

func (m *HandleUplinkMetaDataRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HandleUplinkMetaDataRequest.Unmarshal(m, b)
}
func (m *HandleUplinkMetaDataRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HandleUplinkMetaDataRequest.Marshal(b, m, deterministic)
}
func (m *HandleUplinkMetaDataRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HandleUplinkMetaDataRequest.Merge(m, src)
}
func (m *HandleUplinkMetaDataRequest) XXX_Size() int {
	return xxx_messageInfo_HandleUplinkMetaDataRequest.Size(m)
}
func (m *HandleUplinkMetaDataRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_HandleUplinkMetaDataRequest.DiscardUnknown(m)
}

var xxx_messageInfo_HandleUplinkMetaDataRequest proto.InternalMessageInfo

func (m *HandleUplinkMetaDataRequest) GetDevEui() []byte {
	if m != nil {
		return m.DevEui
	}
	return nil
}

func (m *HandleUplinkMetaDataRequest) GetTxInfo() *gw.UplinkTXInfo {
	if m != nil {
		return m.TxInfo
	}
	return nil
}

func (m *HandleUplinkMetaDataRequest) GetRxInfo() []*gw.UplinkRXInfo {
	if m != nil {
		return m.RxInfo
	}
	return nil
}

func (m *HandleUplinkMetaDataRequest) GetPhyPayloadByteCount() uint32 {
	if m != nil {
		return m.PhyPayloadByteCount
	}
	return 0
}

func (m *HandleUplinkMetaDataRequest) GetMacCommandByteCount() uint32 {
	if m != nil {
		return m.MacCommandByteCount
	}
	return 0
}

func (m *HandleUplinkMetaDataRequest) GetApplicationPayloadByteCount() uint32 {
	if m != nil {
		return m.ApplicationPayloadByteCount
	}
	return 0
}

func (m *HandleUplinkMetaDataRequest) GetMessageType() MType {
	if m != nil {
		return m.MessageType
	}
	return MType_UNKNOWN
}

type HandleDownlinkMetaDataRequest struct {
	// Device EUI (8 bytes).
	DevEui []byte `protobuf:"bytes,1,opt,name=dev_eui,json=devEui,proto3" json:"dev_eui,omitempty"`
	// Multicast Group ID (UUID).
	MulticastGroupId []byte `protobuf:"bytes,2,opt,name=multicast_group_id,json=multicastGroupId,proto3" json:"multicast_group_id,omitempty"`
	// TX meta-data.
	TxInfo *gw.DownlinkTXInfo `protobuf:"bytes,3,opt,name=tx_info,json=txInfo,proto3" json:"tx_info,omitempty"`
	// PHYPayload byte count.
	PhyPayloadByteCount uint32 `protobuf:"varint,4,opt,name=phy_payload_byte_count,json=phyPayloadByteCount,proto3" json:"phy_payload_byte_count,omitempty"`
	// MAC-Command byte count.
	MacCommandByteCount uint32 `protobuf:"varint,5,opt,name=mac_command_byte_count,json=macCommandByteCount,proto3" json:"mac_command_byte_count,omitempty"`
	// Application payload byte count.
	ApplicationPayloadByteCount uint32 `protobuf:"varint,6,opt,name=application_payload_byte_count,json=applicationPayloadByteCount,proto3" json:"application_payload_byte_count,omitempty"`
	// Message type.
	MessageType MType `protobuf:"varint,7,opt,name=message_type,json=messageType,proto3,enum=nc.MType" json:"message_type,omitempty"`
	// Gateway ID.
	GatewayId            []byte   `protobuf:"bytes,8,opt,name=gateway_id,json=gatewayId,proto3" json:"gateway_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HandleDownlinkMetaDataRequest) Reset()         { *m = HandleDownlinkMetaDataRequest{} }
func (m *HandleDownlinkMetaDataRequest) String() string { return proto.CompactTextString(m) }
func (*HandleDownlinkMetaDataRequest) ProtoMessage()    {}
func (*HandleDownlinkMetaDataRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_7dd10f8ea2e14d19, []int{1}
}

func (m *HandleDownlinkMetaDataRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HandleDownlinkMetaDataRequest.Unmarshal(m, b)
}
func (m *HandleDownlinkMetaDataRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HandleDownlinkMetaDataRequest.Marshal(b, m, deterministic)
}
func (m *HandleDownlinkMetaDataRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HandleDownlinkMetaDataRequest.Merge(m, src)
}
func (m *HandleDownlinkMetaDataRequest) XXX_Size() int {
	return xxx_messageInfo_HandleDownlinkMetaDataRequest.Size(m)
}
func (m *HandleDownlinkMetaDataRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_HandleDownlinkMetaDataRequest.DiscardUnknown(m)
}

var xxx_messageInfo_HandleDownlinkMetaDataRequest proto.InternalMessageInfo

func (m *HandleDownlinkMetaDataRequest) GetDevEui() []byte {
	if m != nil {
		return m.DevEui
	}
	return nil
}

func (m *HandleDownlinkMetaDataRequest) GetMulticastGroupId() []byte {
	if m != nil {
		return m.MulticastGroupId
	}
	return nil
}

func (m *HandleDownlinkMetaDataRequest) GetTxInfo() *gw.DownlinkTXInfo {
	if m != nil {
		return m.TxInfo
	}
	return nil
}

func (m *HandleDownlinkMetaDataRequest) GetPhyPayloadByteCount() uint32 {
	if m != nil {
		return m.PhyPayloadByteCount
	}
	return 0
}

func (m *HandleDownlinkMetaDataRequest) GetMacCommandByteCount() uint32 {
	if m != nil {
		return m.MacCommandByteCount
	}
	return 0
}

func (m *HandleDownlinkMetaDataRequest) GetApplicationPayloadByteCount() uint32 {
	if m != nil {
		return m.ApplicationPayloadByteCount
	}
	return 0
}

func (m *HandleDownlinkMetaDataRequest) GetMessageType() MType {
	if m != nil {
		return m.MessageType
	}
	return MType_UNKNOWN
}

func (m *HandleDownlinkMetaDataRequest) GetGatewayId() []byte {
	if m != nil {
		return m.GatewayId
	}
	return nil
}

type HandleUplinkMACCommandRequest struct {
	// Device EUI (8 bytes).
	DevEui []byte `protobuf:"bytes,1,opt,name=dev_eui,json=devEui,proto3" json:"dev_eui,omitempty"`
	// Command identifier (specified by the LoRaWAN specs).
	Cid uint32 `protobuf:"varint,2,opt,name=cid,proto3" json:"cid,omitempty"`
	// MAC-command payload(s).
	Commands             [][]byte `protobuf:"bytes,6,rep,name=commands,proto3" json:"commands,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *HandleUplinkMACCommandRequest) Reset()         { *m = HandleUplinkMACCommandRequest{} }
func (m *HandleUplinkMACCommandRequest) String() string { return proto.CompactTextString(m) }
func (*HandleUplinkMACCommandRequest) ProtoMessage()    {}
func (*HandleUplinkMACCommandRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_7dd10f8ea2e14d19, []int{2}
}

func (m *HandleUplinkMACCommandRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HandleUplinkMACCommandRequest.Unmarshal(m, b)
}
func (m *HandleUplinkMACCommandRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HandleUplinkMACCommandRequest.Marshal(b, m, deterministic)
}
func (m *HandleUplinkMACCommandRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HandleUplinkMACCommandRequest.Merge(m, src)
}
func (m *HandleUplinkMACCommandRequest) XXX_Size() int {
	return xxx_messageInfo_HandleUplinkMACCommandRequest.Size(m)
}
func (m *HandleUplinkMACCommandRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_HandleUplinkMACCommandRequest.DiscardUnknown(m)
}

var xxx_messageInfo_HandleUplinkMACCommandRequest proto.InternalMessageInfo

func (m *HandleUplinkMACCommandRequest) GetDevEui() []byte {
	if m != nil {
		return m.DevEui
	}
	return nil
}

func (m *HandleUplinkMACCommandRequest) GetCid() uint32 {
	if m != nil {
		return m.Cid
	}
	return 0
}

func (m *HandleUplinkMACCommandRequest) GetCommands() [][]byte {
	if m != nil {
		return m.Commands
	}
	return nil
}

type HandleRejectedUplinkFrameSetRequest struct {
	FrameSet             *gw.UplinkFrameSet `protobuf:"bytes,1,opt,name=frame_set,json=frameSet,proto3" json:"frame_set,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *HandleRejectedUplinkFrameSetRequest) Reset()         { *m = HandleRejectedUplinkFrameSetRequest{} }
func (m *HandleRejectedUplinkFrameSetRequest) String() string { return proto.CompactTextString(m) }
func (*HandleRejectedUplinkFrameSetRequest) ProtoMessage()    {}
func (*HandleRejectedUplinkFrameSetRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_7dd10f8ea2e14d19, []int{3}
}

func (m *HandleRejectedUplinkFrameSetRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_HandleRejectedUplinkFrameSetRequest.Unmarshal(m, b)
}
func (m *HandleRejectedUplinkFrameSetRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_HandleRejectedUplinkFrameSetRequest.Marshal(b, m, deterministic)
}
func (m *HandleRejectedUplinkFrameSetRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_HandleRejectedUplinkFrameSetRequest.Merge(m, src)
}
func (m *HandleRejectedUplinkFrameSetRequest) XXX_Size() int {
	return xxx_messageInfo_HandleRejectedUplinkFrameSetRequest.Size(m)
}
func (m *HandleRejectedUplinkFrameSetRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_HandleRejectedUplinkFrameSetRequest.DiscardUnknown(m)
}

var xxx_messageInfo_HandleRejectedUplinkFrameSetRequest proto.InternalMessageInfo

func (m *HandleRejectedUplinkFrameSetRequest) GetFrameSet() *gw.UplinkFrameSet {
	if m != nil {
		return m.FrameSet
	}
	return nil
}

func init() {
	proto.RegisterEnum("nc.MType", MType_name, MType_value)
	proto.RegisterType((*HandleUplinkMetaDataRequest)(nil), "nc.HandleUplinkMetaDataRequest")
	proto.RegisterType((*HandleDownlinkMetaDataRequest)(nil), "nc.HandleDownlinkMetaDataRequest")
	proto.RegisterType((*HandleUplinkMACCommandRequest)(nil), "nc.HandleUplinkMACCommandRequest")
	proto.RegisterType((*HandleRejectedUplinkFrameSetRequest)(nil), "nc.HandleRejectedUplinkFrameSetRequest")
}

func init() {
	proto.RegisterFile("nc/nc.proto", fileDescriptor_7dd10f8ea2e14d19)
}

var fileDescriptor_7dd10f8ea2e14d19 = []byte{
	// 695 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xdc, 0x54, 0xdd, 0x76, 0xd2, 0x4c,
	0x14, 0x2d, 0xd0, 0x42, 0x3b, 0xd0, 0x7e, 0xf9, 0xa6, 0x7f, 0x48, 0xad, 0x22, 0x5e, 0x88, 0x5a,
	0x93, 0xb5, 0xda, 0x17, 0x90, 0x06, 0xaa, 0xe8, 0x2a, 0x6d, 0x53, 0x50, 0x97, 0x37, 0x59, 0xc3,
	0xe4, 0x90, 0xc6, 0x26, 0x33, 0x31, 0x4c, 0x4a, 0xf3, 0x14, 0x3e, 0x8a, 0xcf, 0xe5, 0x4b, 0xb8,
	0x5c, 0x93, 0x84, 0xd2, 0x1f, 0x8a, 0x7a, 0xeb, 0x15, 0x99, 0x39, 0xfb, 0xec, 0x1d, 0xf6, 0x3e,
	0x39, 0xa8, 0xc8, 0xa8, 0xc6, 0xa8, 0xea, 0x07, 0x5c, 0x70, 0x9c, 0x65, 0xb4, 0xb2, 0x65, 0x73,
	0x6e, 0xbb, 0xa0, 0xc5, 0x37, 0xfd, 0x70, 0xa0, 0x81, 0xe7, 0x8b, 0x28, 0x01, 0x54, 0x8a, 0xf6,
	0x48, 0xb3, 0x47, 0xc9, 0xa1, 0xf6, 0x23, 0x8b, 0xb6, 0xde, 0x12, 0x66, 0xb9, 0xd0, 0xf3, 0x5d,
	0x87, 0x9d, 0x1f, 0x82, 0x20, 0x4d, 0x22, 0x88, 0x01, 0x5f, 0x43, 0x18, 0x0a, 0xbc, 0x89, 0x0a,
	0x16, 0x5c, 0x98, 0x10, 0x3a, 0xe5, 0x4c, 0x35, 0x53, 0x2f, 0x19, 0x79, 0x0b, 0x2e, 0x5a, 0xa1,
	0x83, 0x9f, 0xa3, 0x82, 0xb8, 0x34, 0x1d, 0x36, 0xe0, 0xe5, 0x6c, 0x35, 0x53, 0x2f, 0xee, 0x2a,
	0xaa, 0x3d, 0x52, 0x13, 0x92, 0xee, 0xa7, 0x36, 0x1b, 0x70, 0x23, 0x2f, 0x2e, 0xe5, 0xaf, 0x84,
	0x06, 0x29, 0x34, 0x57, 0xcd, 0xdd, 0x84, 0x1a, 0x29, 0x34, 0x48, 0xa0, 0x7b, 0x68, 0xc3, 0x3f,
	0x8b, 0x4c, 0x9f, 0x44, 0x2e, 0x27, 0x96, 0xd9, 0x8f, 0x04, 0x98, 0x94, 0x87, 0x4c, 0x94, 0xe7,
	0xab, 0x99, 0xfa, 0xb2, 0xb1, 0xea, 0x9f, 0x45, 0xc7, 0x49, 0x71, 0x3f, 0x12, 0xa0, 0xcb, 0x92,
	0x6c, 0xf2, 0x08, 0x35, 0x29, 0xf7, 0x3c, 0xc2, 0x6e, 0x34, 0x2d, 0x24, 0x4d, 0x1e, 0xa1, 0x7a,
	0x52, 0x9c, 0x34, 0xe9, 0xe8, 0x11, 0xf1, 0x7d, 0xd7, 0xa1, 0x44, 0x38, 0x9c, 0x4d, 0x53, 0xcc,
	0xc7, 0xcd, 0x5b, 0xd7, 0x50, 0x77, 0x94, 0x77, 0x50, 0xc9, 0x83, 0xe1, 0x90, 0xd8, 0x60, 0x8a,
	0xc8, 0x87, 0x72, 0xa1, 0x9a, 0xa9, 0xaf, 0xec, 0x2e, 0xa9, 0x8c, 0xaa, 0x87, 0xdd, 0xc8, 0x07,
	0xa3, 0x98, 0x96, 0xe5, 0xa1, 0xf6, 0x2d, 0x87, 0xb6, 0x13, 0xaf, 0x9b, 0x7c, 0xc4, 0xfe, 0xca,
	0xed, 0x1d, 0x84, 0xbd, 0xd0, 0x15, 0x0e, 0x25, 0x43, 0x61, 0xda, 0x01, 0x0f, 0x7d, 0xd3, 0xb1,
	0x62, 0xe3, 0x4b, 0x86, 0x72, 0x55, 0x79, 0x23, 0x0b, 0x6d, 0x0b, 0xbf, 0x9c, 0x64, 0x93, 0x8b,
	0xb3, 0xc1, 0xd2, 0xf0, 0xb1, 0xe8, 0xad, 0x74, 0xfe, 0x65, 0xcb, 0xf1, 0x36, 0x42, 0x36, 0x11,
	0x30, 0x22, 0x91, 0xf4, 0x6b, 0x31, 0xf6, 0x6b, 0x29, 0xbd, 0x69, 0x5b, 0xb5, 0xc1, 0x38, 0x90,
	0x74, 0xf8, 0x1b, 0x7a, 0xfa, 0xd2, 0xbf, 0x0d, 0x44, 0x41, 0x39, 0x9a, 0x26, 0xb0, 0x6c, 0xc8,
	0x47, 0x5c, 0x41, 0x8b, 0xa9, 0x1d, 0xc3, 0x72, 0xbe, 0x9a, 0xab, 0x97, 0x8c, 0xab, 0x73, 0xed,
	0x03, 0x7a, 0x9a, 0xe8, 0x18, 0xf0, 0x05, 0xa8, 0x00, 0x2b, 0xd1, 0x3b, 0x08, 0x88, 0x07, 0xa7,
	0x20, 0xc6, 0x6a, 0x1a, 0x5a, 0x1a, 0xc8, 0x2b, 0x73, 0x08, 0x22, 0xd6, 0x4b, 0x93, 0xbb, 0x85,
	0x5e, 0x1c, 0xa4, 0x4f, 0x2f, 0xbe, 0x67, 0xd0, 0x42, 0xfc, 0xaf, 0x71, 0x11, 0x15, 0x7a, 0x9d,
	0xf7, 0x9d, 0xa3, 0x8f, 0x1d, 0x65, 0x0e, 0x2b, 0xa8, 0xf4, 0xee, 0xa8, 0xdd, 0x31, 0x8d, 0xd6,
	0x49, 0xaf, 0x75, 0xda, 0x55, 0x32, 0xf8, 0x3f, 0x54, 0x8c, 0x6f, 0x1a, 0xba, 0xde, 0x3a, 0xee,
	0x2a, 0x59, 0xbc, 0x89, 0x56, 0x7b, 0x1d, 0xfd, 0xa8, 0x73, 0xd0, 0x36, 0x0e, 0x5b, 0x4d, 0xb3,
	0xd9, 0xe8, 0x36, 0xcc, 0xde, 0xb1, 0x92, 0xc3, 0x0f, 0xd0, 0xfa, 0x9d, 0x42, 0x53, 0xd2, 0xce,
	0xe3, 0x75, 0xf4, 0xff, 0xdd, 0x8e, 0x05, 0x49, 0x35, 0x0d, 0x9f, 0xc7, 0x18, 0xad, 0x18, 0xad,
	0x1b, 0x2f, 0x52, 0xd8, 0xfd, 0x99, 0x45, 0xe5, 0x0e, 0x88, 0x11, 0x0f, 0xce, 0x75, 0xce, 0x44,
	0xc0, 0x5d, 0x17, 0x82, 0x53, 0x08, 0x2e, 0x1c, 0x0a, 0xf8, 0x04, 0xad, 0x4d, 0xdb, 0x45, 0xf8,
	0xb1, 0x4c, 0x77, 0xc6, 0x96, 0xaa, 0x6c, 0xa8, 0xc9, 0xc2, 0x53, 0xc7, 0x0b, 0x4f, 0x6d, 0xc9,
	0x85, 0x57, 0x9b, 0xc3, 0x3d, 0xb4, 0x31, 0xfd, 0x93, 0xc3, 0x4f, 0x26, 0xa4, 0xf7, 0x7c, 0x8e,
	0x7f, 0x42, 0x7b, 0x7b, 0x70, 0xae, 0xd3, 0xde, 0x33, 0x54, 0x33, 0x68, 0x09, 0x7a, 0x38, 0x6b,
	0x4e, 0xf0, 0xb3, 0x09, 0xf9, 0xcc, 0x49, 0xba, 0x5f, 0x62, 0xff, 0x35, 0x5a, 0x73, 0xb8, 0x4a,
	0xcf, 0x9c, 0xc0, 0x1f, 0x0a, 0x42, 0xcf, 0x55, 0xe2, 0x3b, 0x2a, 0xa3, 0x9f, 0xeb, 0xb6, 0x23,
	0x00, 0x54, 0xca, 0x3d, 0xcd, 0x1a, 0x04, 0xbc, 0xcf, 0x05, 0xb5, 0xb4, 0x09, 0xec, 0x15, 0xf1,
	0x1d, 0xcd, 0xe6, 0x1a, 0xa3, 0xfd, 0x7c, 0xcc, 0xb9, 0xf7, 0x2b, 0x00, 0x00, 0xff, 0xff, 0x8d,
	0xff, 0x69, 0x4c, 0x76, 0x06, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// NetworkControllerServiceClient is the client API for NetworkControllerService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type NetworkControllerServiceClient interface {
	// HandleUplinkMetaData handles uplink meta-rata.
	HandleUplinkMetaData(ctx context.Context, in *HandleUplinkMetaDataRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// HandleDownlinkMetaData handles downlink meta-data.
	HandleDownlinkMetaData(ctx context.Context, in *HandleDownlinkMetaDataRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// HandleUplinkMACCommand handles an uplink mac-command.
	// This method will only be called in case the mac-command request was
	// enqueued throught the API or when the CID is >= 0x80 (proprietary
	// mac-command range).
	HandleUplinkMACCommand(ctx context.Context, in *HandleUplinkMACCommandRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
	// HandleRejectedUplinkFrameSet handles a rejected uplink.
	// And uplink can be rejected in the case the device has not (yet) been
	// provisioned, because of invalid frame-counter, MIC, ...
	HandleRejectedUplinkFrameSet(ctx context.Context, in *HandleRejectedUplinkFrameSetRequest, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type networkControllerServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewNetworkControllerServiceClient(cc grpc.ClientConnInterface) NetworkControllerServiceClient {
	return &networkControllerServiceClient{cc}
}

func (c *networkControllerServiceClient) HandleUplinkMetaData(ctx context.Context, in *HandleUplinkMetaDataRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/nc.NetworkControllerService/HandleUplinkMetaData", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *networkControllerServiceClient) HandleDownlinkMetaData(ctx context.Context, in *HandleDownlinkMetaDataRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/nc.NetworkControllerService/HandleDownlinkMetaData", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *networkControllerServiceClient) HandleUplinkMACCommand(ctx context.Context, in *HandleUplinkMACCommandRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/nc.NetworkControllerService/HandleUplinkMACCommand", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *networkControllerServiceClient) HandleRejectedUplinkFrameSet(ctx context.Context, in *HandleRejectedUplinkFrameSetRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/nc.NetworkControllerService/HandleRejectedUplinkFrameSet", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// NetworkControllerServiceServer is the server API for NetworkControllerService service.
type NetworkControllerServiceServer interface {
	// HandleUplinkMetaData handles uplink meta-rata.
	HandleUplinkMetaData(context.Context, *HandleUplinkMetaDataRequest) (*emptypb.Empty, error)
	// HandleDownlinkMetaData handles downlink meta-data.
	HandleDownlinkMetaData(context.Context, *HandleDownlinkMetaDataRequest) (*emptypb.Empty, error)
	// HandleUplinkMACCommand handles an uplink mac-command.
	// This method will only be called in case the mac-command request was
	// enqueued throught the API or when the CID is >= 0x80 (proprietary
	// mac-command range).
	HandleUplinkMACCommand(context.Context, *HandleUplinkMACCommandRequest) (*emptypb.Empty, error)
	// HandleRejectedUplinkFrameSet handles a rejected uplink.
	// And uplink can be rejected in the case the device has not (yet) been
	// provisioned, because of invalid frame-counter, MIC, ...
	HandleRejectedUplinkFrameSet(context.Context, *HandleRejectedUplinkFrameSetRequest) (*emptypb.Empty, error)
}

// UnimplementedNetworkControllerServiceServer can be embedded to have forward compatible implementations.
type UnimplementedNetworkControllerServiceServer struct {
}

func (*UnimplementedNetworkControllerServiceServer) HandleUplinkMetaData(ctx context.Context, req *HandleUplinkMetaDataRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method HandleUplinkMetaData not implemented")
}
func (*UnimplementedNetworkControllerServiceServer) HandleDownlinkMetaData(ctx context.Context, req *HandleDownlinkMetaDataRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method HandleDownlinkMetaData not implemented")
}
func (*UnimplementedNetworkControllerServiceServer) HandleUplinkMACCommand(ctx context.Context, req *HandleUplinkMACCommandRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method HandleUplinkMACCommand not implemented")
}
func (*UnimplementedNetworkControllerServiceServer) HandleRejectedUplinkFrameSet(ctx context.Context, req *HandleRejectedUplinkFrameSetRequest) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method HandleRejectedUplinkFrameSet not implemented")
}

func RegisterNetworkControllerServiceServer(s *grpc.Server, srv NetworkControllerServiceServer) {
	s.RegisterService(&_NetworkControllerService_serviceDesc, srv)
}

func _NetworkControllerService_HandleUplinkMetaData_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HandleUplinkMetaDataRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NetworkControllerServiceServer).HandleUplinkMetaData(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nc.NetworkControllerService/HandleUplinkMetaData",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NetworkControllerServiceServer).HandleUplinkMetaData(ctx, req.(*HandleUplinkMetaDataRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _NetworkControllerService_HandleDownlinkMetaData_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HandleDownlinkMetaDataRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NetworkControllerServiceServer).HandleDownlinkMetaData(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nc.NetworkControllerService/HandleDownlinkMetaData",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NetworkControllerServiceServer).HandleDownlinkMetaData(ctx, req.(*HandleDownlinkMetaDataRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _NetworkControllerService_HandleUplinkMACCommand_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HandleUplinkMACCommandRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NetworkControllerServiceServer).HandleUplinkMACCommand(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nc.NetworkControllerService/HandleUplinkMACCommand",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NetworkControllerServiceServer).HandleUplinkMACCommand(ctx, req.(*HandleUplinkMACCommandRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _NetworkControllerService_HandleRejectedUplinkFrameSet_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HandleRejectedUplinkFrameSetRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NetworkControllerServiceServer).HandleRejectedUplinkFrameSet(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/nc.NetworkControllerService/HandleRejectedUplinkFrameSet",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NetworkControllerServiceServer).HandleRejectedUplinkFrameSet(ctx, req.(*HandleRejectedUplinkFrameSetRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _NetworkControllerService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "nc.NetworkControllerService",
	HandlerType: (*NetworkControllerServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "HandleUplinkMetaData",
			Handler:    _NetworkControllerService_HandleUplinkMetaData_Handler,
		},
		{
			MethodName: "HandleDownlinkMetaData",
			Handler:    _NetworkControllerService_HandleDownlinkMetaData_Handler,
		},
		{
			MethodName: "HandleUplinkMACCommand",
			Handler:    _NetworkControllerService_HandleUplinkMACCommand_Handler,
		},
		{
			MethodName: "HandleRejectedUplinkFrameSet",
			Handler:    _NetworkControllerService_HandleRejectedUplinkFrameSet_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "nc/nc.proto",
}
